#ifndef __LIB_H
#define __LIB_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void swap(void* p1, void* p2, size_t data_size);
void qsort(void* data, size_t total_size, size_t data_size, int (*compare)(const void*, const void*));

#endif
