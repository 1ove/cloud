#include <stdio.h>

bool circularArrayLoop(int* nums, int numsSize) {
    if(!nums || numsSize <= 1) {
        return false;
    }

    int step = 0;
    int cur = numsSize-1;
    int next = 0;

    while(cur>=0) {
        next = (cur+nums[cur])%numsSize;
        if(nums[next] == numsSize || nums[next] == 0) {
            nums[next] = 0;
        } else {
            step++;
        }
        cur--;
    }
}

int main() {
    const int SIZE = 5;
    int array[SIZE] = {2, -1, 1, 2, 2};
    printf("%d", circularArrayLoop(array, SIZE));
    return 0;
}
