#include <stdio.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode* reverseList(struct ListNode* head) {
    if(!head) return NULL;
    struct ListNode* p1;
    struct ListNode* p2;
    struct ListNode* p3;

    p1 = NULL;
    p2 = p3 = head;
    while(p3->next) {
        p3 = p3->next;
        p2->next = p1;
        p1 = p2;
        p2 = p3;
    }
    p2->next = p1;

    return p2;
}

int main() {
    return 0;
}
