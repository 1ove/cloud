#include <stdio.h>

void swap(void* p1, void* p2, size_t data_size) {
    void* tmp = malloc(data_size);
    memcpy(tmp, p1, data_size);
    memcpy(p1, p2, data_size);
    memcpy(p2, tmp, data_size);
    free(tmp);
}

int compare_int(const void* p1, const void* p2) {
    int* a = (int*)(p1);
    int* b = (int*)(p2);
    return *a>*b;
}

void print_int_array(int* a, int size) {
    int i;
    for(i=0;i<size;i++) {
        printf("%d\t", a[i]);
    }
    printf("\n");
}


void bubbleSort(void* data, size_t data_size, int s, int e, int (*compare)(const void*, const void*)) {
    int i,j;

    int length = e-s+1;
    printf("%d\n", length);

    for(i=length-1; i>0; i--) {
        for(j=0;j<i;j++) {
            if(compare((data+j*data_size), data+(j+1)*data_size)) {
                swap(data+j*data_size, data+(j+1)*data_size, data_size);
            } else {
            }
        }
    }
    return;
}

int main() {
    const int array_size = 4;
    int a[5] = {213, -12, -12, -5656};
    bubbleSort(a, sizeof(int), 0, array_size-1, compare_int);
    print_int_array(a, array_size);
    return 0;
}
