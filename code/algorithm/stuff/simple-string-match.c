#include <stdio.h>
#include <string.h>

int basicFindSubstr(const char* string, const char* search) {
    size_t m,i;
    m = i = 0;

    while(m < strlen(string)) {
        if(string[m] == search[i]) {
            if(i == strlen(search)-1) {
                return m-strlen(search)+1;
            } else {
                m++;
                i++;
            }
        } else {
            m = m-i+1;
            i = 0;
        }
    }
    return -1;
}


int main() {
    printf("%d\n", basicFindSubstr("AAAAAAB", "AB"));
    return 0;
}
