#include <unistd.h>
#include <stdio.h>

int main(int argc, char **argv) {
    int pfd[2];
    const int BUF_SIZE = 1024;
    void *buf = malloc(BUF_SIZE);
    memset(buf, '\0', BUF_SIZE);

    if(0 == pipe(pfd)) {
        pid_t pid = fork();
        if(0 == pid) {
            close(pfd[1]);
            read(pfd[0], buf, BUF_SIZE);
            printf("%s\n", buf);
            exit(0);
        } else {
            close(pfd[0]);
            write(pfd[1], "Hello World!", BUF_SIZE);
            exit(0);
        }
    } else {
        printf("Create pipe failed!");
    }
    printf("End\n");
    //free(buf);
    return 0;
}
