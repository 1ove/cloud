/**
 * @desc 声明一批线程，0号线程为master线程，其他线程为worker线程
 *       master线程随机的给worker线程传递一些消息，其他线程接到请求后按照需要做一些事情
 *       指令的发出是通过管道来的
 */

#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<errno.h>
#include<sys/types.h>
#include<sys/wait.h>

#define WORKER_NUM 2

pthread_t master;                // master 线程
pthread_t worker[WORKER_NUM];    // worker线程

int fd[2];                       // 用于master 和 worker 通信的管道

/**
 * @desc 主线程的处理函数
 *       每隔1秒钟往worker线程中发送指令
 */
void* masterThreadProc(void* arg)
{
    printf("I am master\n");
    char* pBuffer = "hello worker";
    int len = strlen(pBuffer);

    write(fd[1], pBuffer, len);
    printf("write %d: %s\n", fd[1], pBuffer);

    return NULL;
}

void* workerThreadProc(void* arg)
{
    sleep(6);

    printf("I am worker\n");
    char pBuffer[1024];
    read(fd[0], pBuffer, 1024);
    printf("read %d :%s\n", fd[0], pBuffer);

    return NULL;
}

int main(int argc, char** argv)
{
    // 初始化管道
    if (pipe(fd)) 
    {
        printf("create pipe error\n");
        return -1;
    }

    printf("fd[0] : %d fd[1] : %d\n", fd[0], fd[1]);

    // 初始化master线程
    int ret = pthread_create(&master, NULL, masterThreadProc, NULL);
    if (-1 == ret) 
    {
        printf("fail to create master\n");
        return -1;
    }

    // 初始化worker线程
    int i;
    for(i = 0; i < WORKER_NUM; i++)
    {
        ret = pthread_create(&worker[i], NULL, workerThreadProc, NULL);
        if (-1 == ret) 
        {
            printf("fail to create worker [%d]\n", i);
        }
    }

    sleep(10);

    return 0;
}
