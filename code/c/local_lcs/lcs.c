#include "lcs.h"

static char* port;
static int ret;

int init() {
    pid_t pid = getpid();
    fprintf(stdout, "Server started, pid[%d]\n", pid);
    return 0;
}

int main(int argc, char *argv[]) {
    int myerrno = 0;
    char* errmsg = (char*)malloc(1024);
    memset(errmsg, 0, 1024);

    port = strdup(LCS_DEFAULT_PORT);
    if(argc > 1) {
        port = strdup(argv[1]);
    }

    // read conf
    // todo

    /* read opt */
    //todo

    init();
    create_server(port, &myerrno, errmsg);
    fprintf(stdout, "Failed to create a server, Errmsg : [%s]\n", errmsg);

    free(errmsg);
    free(port);
    return EXIT_SUCCESS;
}
