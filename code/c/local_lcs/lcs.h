/***************************************************************************
 * 
 * Copyright (c) 2016 example.com, Inc. All Rights Reserved
 * 
 **************************************************************************/

/**
 * @file lcs.h
 * @author yaokun(com@example.com)
 * @date 2016/05/20 11:26:36
 * @brief 
 *  
 **/

#ifndef  __LCS_H_
#define  __LCS_H_

#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>

#include "const.h"
#include "LcsLog.h"

#endif  //__LCS_H_
