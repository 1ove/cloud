/***************************************************************************
 * 
 * Copyright (c) 2016 example.com, Inc. All Rights Reserved
 * 
 **************************************************************************/

/**
 * @file ConnentManager.h
 * @author yaokun(com@example.com)
 * @date 2016/05/20 17:05:45
 * @brief 
 *  
 **/

#ifndef  __CONNENTMANAGER_H_
#define  __CONNENTMANAGER_H_

#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <error.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "const.h"

#include "LcsLog.h"

typedef struct _Connect {
    short status;
    short type;
    //int (*protocol_header)(char* buf, const char* header); // 协议
} Connect;

enum ThreadStatus {
    INIT,
    RUNNING
};

typedef struct _Thread {
    int epoll_fd;
    struct epoll_event event;
    struct epoll_event* events;
    unsigned char status;
} Thread;

int create_server(const char* port, int* myerrno, char* errmsg);

#endif  //__CONNENTMANAGER_H_
