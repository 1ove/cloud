/***************************************************************************
 * 
 * Copyright (c) 2016 example.com, Inc. All Rights Reserved
 * 
 **************************************************************************/
 
/**
 * @file const.h
 * @author yaokun(com@example.com)
 * @date 2016/05/20 16:45:08
 * @brief 
 *  
 **/

#ifndef  __CONST_H_
#define  __CONST_H_

#define MAXEVENTS 1024
#define MAX_BUF_SIZE 1024*1024
#define MAX_CONNECT 1024*1024

#define CONNECT_LOG_PATH "."
#define CONNECT_LOG_WARNING_FILENAME "connect.log.wf"

#define MAX_LOG_FILENAME_LEN 200
#define MAX_THREAD 2048

#define CONNECT_STATUS_ERROR 1
#define CONNECT_STATUS_DONE 0

#define LCS_DEFAULT_PORT "8989"

#endif  //__CONST_H_
