#!/bin/sh

GIT_PATH="https://gitee.com/1ove/cloud.git"
GIT_HUGE_PATH="https://gitee.com/1ove/cloud_huge.git"
REMOTE_HOST="http://www.morysky.win"

REMOTE_HUGE_PATH=$REMOTE_HOST"/static/git_sync/cloud_huge/vim"
REMOTE_PATH=$REMOTE_HOST"/static/git_sync/cloud/general/vim"
CORE_TAR_FILENAME="bundle.tar.bz2"

while getopts "r:h" opt; do
    case $opt in
        r )  echo "Setting your remote host : [$OPTARG]"; REMOTE_HUGE_PATH=$OPTARG;;
        h )  echo "Usage : -r bundle_remote_host" ;;
        \?)  echo "Usage : -r bundle_remote_host" ;;
    esac
done

#USER_PWD=`pwd`
#CUR=`dirname $0`
CUR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $CUR

#wget $REMOTE_PATH/vimrc
if test -d cloud; then
    cd cloud
    git pull
    cd ..
else
    git clone $GIT_PATH
fi

cp cloud/general/vim/vimrc ~/.vimrc

if [ -d ~/.vim ]; then
    :
else
    mkdir ~/.vim
fi

if test -d cloud_huge ;
then
    cd cloud_huge
    git pull
    cd ..
    :
else
    #wget $REMOTE_HUGE_PATH/$CORE_TAR_FILENAME
    git clone $GIT_HUGE_PATH
fi
cp "cloud_huge/vim/"$CORE_TAR_FILENAME ~/.vim/
cd ~/.vim
rm -rf bundle
tar xjf $CORE_TAR_FILENAME
rm -rf $CORE_TAR_FILENAME

cd $USER_PWD
echo "Install Done!";
