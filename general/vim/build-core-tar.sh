BUNDLE_PATH="$HOME/.vim/bundle"
TAR_FILENAME="bundle.tar.bz2"
TMP_PATH="/tmp/build-core-vim-tar"

real_script_path=`pwd`

# Clean
rm -rf $TAR_FILENAME

vim +PluginInstall +qall

mkdir -p $TMP_PATH
cd $TMP_PATH
cp -r $BUNDLE_PATH bundle

# cd bundle
# mv Vundle.vim ../
# rm -rf *
# mv ../Vundle.vim .
# cd $TMP_PATH

tar cjf $TAR_FILENAME bundle

cd $real_script_path
mv "$TMP_PATH/$TAR_FILENAME" .

# Clean
rm -rf $TMP_PATH
