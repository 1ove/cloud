#!/bin/sh

GIT_HUGE_PATH="https://gitee.com/1ove/cloud_huge.git"
REMOTE_HUGE_PATH="http://www.morysky.win"

REMOTE_HUGE_PATH=$REMOTE_HUGE_PATH"/static/git_sync/cloud_huge/vim"
TAR_SUFFIX="tar.bz2"

while getopts "r:h" opt; do
    case $opt in
        r )  echo "Setting your remote host : [$OPTARG]"; REMOTE_HUGE_PATH=$OPTARG;;
        h )  echo "Usage : -r bundle_remote_host" ;;
        \?)  echo "Usage : -r bundle_remote_host" ;;
    esac
done

CUR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $CUR

if test -d cloud_huge; then
    cd cloud_huge
    git pull
    cd ..
else
    :
fi

ext_list=(YouCompleteMe neocomplete.vim)
script_real_path=`pwd`

pwd
for ext_name in ${ext_list[@]}
do
    #if [ ! -f $ext_name.$TAR_SUFFIX ]; then
    #    wget $REMOTE_HUGE_PATH/$ext_name.$TAR_SUFFIX
    #fi
    cp "cloud_huge/vim/"$ext_name.$TAR_SUFFIX ~/.vim/bundle
    cd ~/.vim/bundle
    tar xvjf $ext_name.$TAR_SUFFIX
    rm -rf $ext_name.$TAR_SUFFIX
    cd $script_real_path
    rm -rf $ext_name.$TAR_SUFFIX
done

cd $script_real_path
