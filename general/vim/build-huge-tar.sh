#!/bin/sh

cd ~/.vim/bundle

rm -rf YouCompleteMe/.git
rm -rf YouCompleteMe/third_party/ycmd/third_party/OmniSharpServer
tar cvjf YouCompleteMe.tar.bz2 YouCompleteMe

tar cvjf neocomplete.vim.tar.bz2 neocomplete.vim
