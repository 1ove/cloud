#!/bin/bash

VERSION="2.8"
SOURCE_FILE_NAME=$VERSION".tar.gz"

if [ -f /etc/debian_version ]
then
	apt-get install libevent-dev pkg-config ncurses-dev
fi

if [ -f $SOURCE_FILE_NAME ]
then 
	echo "Source tar exists, skip downloading."
else
	wget https://github.com/tmux/tmux/archive/${SOURCE_FILE_NAME}
fi
tar -xf $SOURCE_FILE_NAME
cd "tmux-"$VERSION
./autogen.sh
# Default path -> /usr/local/bin/tmux, you can copy it to /usr/bin, or set the prefix
./configure
make
make install

cd ~
git clone https://github.com/gpakosz/.tmux.git
ln -s -f .tmux/.tmux.conf
cp .tmux/.tmux.conf.local .
rm -rf .tmux.git
